﻿using HW4;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace HW4
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Player> information = new ObservableCollection<Player>();
        public MainPage()
        {
            InitializeComponent();
            PopulateListView();



        }

        private void ViewCells_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }
        private void PopulateListView()
        {
            //var information = new ObservableCollection<Player>();
            

            var player1 = new Player();
            player1.Image = "carr.png";
            player1.Name = "Derek Carr";
            player1.Height = "6'3";
            player1.college = "Fresno State";
           

            information.Add(player1);

            var player2 = new Player();
            player2.Image = "hudson.png";
            player2.Name = "Rodney Hudson";
            player2.Height = "6'2";
            player2.college = "Florida State University";

            information.Add(player2);

            var player3 = new Player();
            player3.Image = "hurst.png";
            player3.Name = "Maurice Hurst";
            player3.Height = "6'2";
            player3.college = "University of Michigan";

            information.Add(player3);

            var player4 = new Player();
            player4.Image = "joseph.png";
            player4.Name = "Karl Joseph";
            player4.Height = "5'10";
            player4.college = "University of Michigan";
           
            information.Add(player4);

            var player5 = new Player();
            player5.Image = "jelly.png";
            player5.Name = "Justin Ellis";
            player5.Height = "6'2";
            player5.college = "Louisiana Tech";

            information.Add(player5);

            ViewCells.ItemsSource = information;

        }

        private void ViewCells_Refreshing(object sender, EventArgs e)
        {
            PopulateListView();

            ViewCells.IsRefreshing = false;
            
        }

        public void MenuItem_Clicked(object sender, EventArgs e)
        {
            var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
            var deleteInfo = ((MenuItem)sender);
            
            information.Remove((HW4.Player)deleteInfo.CommandParameter);
            
        }
    }
}

    
